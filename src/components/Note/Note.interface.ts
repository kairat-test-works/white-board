// eslint-disable-next-line @typescript-eslint/interface-name-prefix
export default interface INote {
  title: string;
  description: string;
  cls: string;
}
